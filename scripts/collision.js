function wallCollide() {
    for (var i = walls.length - 1; i >= 0; i--) {
        if (pagmanOrigin.x < walls[i].x + walls[i].w &&
            pagmanOrigin.x + pagman.size * 2 > walls[i].x &&
            pagmanOrigin.y < walls[i].y + walls[i].h &&
            pagman.size * 2 + pagmanOrigin.y > walls[i].y) {

            if (pagman.direction == 3 && pagman.y > pagman.size) pagman.y += pagman.speed;
            else if (pagman.direction == 4 && pagman.x < canvas.width - pagman.size) pagman.x -= pagman.speed;
            else if (pagman.direction == 1 && pagman.y < canvas.height - pagman.size) pagman.y -= pagman.speed;
            else if (pagman.direction == 2 && pagman.x > pagman.size) pagman.x += pagman.speed;
        }
    }
}