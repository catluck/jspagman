var wallsXList = [40, 140, 260, 320, 440,
                40, 140, 200, 380, 440,
                0, 160, 260, 320, 440,
                140, 200, 380,
                40, 140, 260, 320, 440,
                0, 80, 200, 380, 500,
                40, 140, 260, 440, 320];
var wallsYList = [40, 40 , 0, 40, 40,
                120, 120, 120, 120, 120, 
                180, 180, 140, 180, 180,
                300, 360, 300,
                420, 420, 380, 420, 420,
                480, 440, 480, 440, 480,
                540, 480, 500, 480, 540];
var wallsWList = [60, 80, 20, 80, 60,
                60, 20, 140, 20, 60,
                100, 60, 20, 60, 100,
                20, 140, 20,
                60, 80, 20, 80, 60,
                40, 20, 140, 20, 40,
                180, 20, 20, 20, 180];
var wallsHList = [40, 40, 80, 40, 40,
                20, 140, 20, 140, 20,
                200, 20, 60, 20, 200,
                80, 20, 80,
                20, 20, 60, 20, 20,
                20, 60, 20, 60, 20,
                20, 60, 60, 60, 20];

var walls = [];
function spawnWalls() {
    for (var i = 0; i < wallsXList.length; i++) {
        walls[i] = new wall(wallsXList[i], wallsYList[i], wallsWList[i], wallsHList[i]);
    }
}

var pagman = {x : 30, y : 20, size : 20, frame : 0, direction : 4, speed : 5};
var pagmanOrigin = {x : pagman.x - pagman.size, y : pagman.y - pagman.size};

function wall(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
}

var dotSize = 5;
var dots = [];
var dots2 = [];
var dotIndex = 0;
function spawnDots() {
    for (var ix = 20; ix < 540; ix += 20) {
        for (var iy = 20; iy < 600; iy += 20) {
            dots[dotIndex] = new dot(ix, iy);

            for (var j = 0; j < walls.length; j++) {
                if (dots[dotIndex].x <= walls[j].x + walls[j].w &&
                    dots[dotIndex].x + dotSize >= walls[j].x &&
                    dots[dotIndex].y <= walls[j].y + walls[j].h &&
                    dots[dotIndex].y + dotSize >= walls[j].y) {
                        dots.splice(dotIndex, 1)
                        dotIndex--;
                        console.log("dupa"); 
                }
            }
            dotIndex++;
        }
    }
}

function dot(x, y) {
    this.x = x;
    this.y = y;
}